package gorilla_test

import (
	"testing"

	"github.com/gorilla/sessions"
	"bitbucket.org/botsolve/tb_deps/session/gorilla"
	"bitbucket.org/botsolve/tb_deps/session/test"
)

func TestAll(t *testing.T) {
	engine := sessions.NewCookieStore([]byte("something-very-secret"))
	manager := gorilla.New("_session", engine)
	test.TestAll(manager, t)
}
