package filesystem

import (
	"testing"

	"bitbucket.org/botsolve/tb_deps/oss/tests"
)

func TestAll(t *testing.T) {
	fileSystem := New("/tmp")
	tests.TestAll(fileSystem, t)
}
